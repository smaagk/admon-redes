# Stage 0, based on Node.js, to build and compile Angular
FROM node:latest as node
WORKDIR /app
COPY ./ /app/
RUN npm install
RUN npm run build 

FROM nginx:latest
COPY --from=node /app/dist/practica1 /usr/share/nginx/html

